count = 99

def plural(count)
  """
    #{count} bottles of beer on the wall, #{count} bottles of beer.
    Take one down and pass it around, #{count - 1} bottle of beer on the wall.
    """
end

def singular
  '''
1 bottle of beer on the wall, 1 bottle of beer.
Take one down and pass it around, no more bottles of beer on the wall.
'''
end

def none
  '''
No more bottles of beer on the wall, no more bottles of beer.
Go to the store and buy some more, 99 bottles of beer on the wall.
'''
end

while count > -1
  result = ''

  result = if count > 1
             plural count
           elsif count == 1
             singular
           else
             none
           end

  puts result
  count -= 1
end
