require 'spec_helper'
require_relative '../lib/bottle_song'

RSpec.describe BottleSong do
  it 'writes the 99 bottles of beer song' do
    song = BottleSong.new
    expect(song.lyrics).to eq '99 bot...'
  end

  describe '#song' do
    it 'returns a verse of the song (99 bottles)' do
      song = BottleSong.new
      expect(song.verse(99)).to eq '99 bottles of beer on the wall, 99 bottles of beer. Take one down and pass it around, 98 bottles of beer on the wall.'
    end

    it 'returns a verse of the song (98 bottles)' do
      song = BottleSong.new
      expect(song.verse(98)).to eq '98 bottles of beer on the wall, 98 bottles of beer. Take one down and pass it around, 97 bottles of beer on the wall.'
    end
  end
end
